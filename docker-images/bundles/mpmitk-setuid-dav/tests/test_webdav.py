import pytest
from webdav_suite import set_up_suite, import_tests

# mod_mpmitk_setuid evaluates the authenticated user after each request and
# potentially performs an irreversible setuid. So we make sure that the
# requests library uses each connection only for one request.
pytestmark = pytest.mark.session_keep_alive(False)

files = [
    {
        'path': '/my-user/content.txt',
        'data': 'accessible data\n',
        'read_accessible': True,
        'write_accessible': True,
    },
    {
        'path': '/my-user-read-accessible/content.txt',
        'data': 'read-accessible data\n',
        'read_accessible': True,
        'write_accessible': False,
    },
    {
        'path': '/group-accessible/content.txt',
        'data': 'accessible data\n',
        'read_accessible': True,
        'write_accessible': True,
    },
    {
        'path': '/other-user/content.txt',
        'data': 'not-accessible data\n',
        'read_accessible': False,
        'write_accessible': False,
    },
]

directories = [
    {
        'path': '/my-user',
        'content': ['content.txt'],
        'read_accessible': True,
        'write_accessible': True,
    },
    {
        'path': '/my-user-read-accessible',
        'content': ['content.txt'],
        'read_accessible': True,
        'write_accessible': False,
    },
    {
        'path': '/group-accessible',
        'content': ['content.txt'],
        'read_accessible': True,
        'write_accessible': True,
    },
    {
        'path': '/other-user',
        'content': ['content.txt'],
        'read_accessible': False,
        'write_accessible': False,
    },
]

set_up_suite(globals(), files, directories)
import_tests(globals())
