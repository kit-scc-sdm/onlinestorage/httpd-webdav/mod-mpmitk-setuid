# mod_mpmitk_setuid

This module allows to switch to different POSIX users (`setuid`/`setgid`) for the handling of each request.
In combination with authentication, the module enables user-specific access conforming to filesystem permissions.

Its primary use case is exposing WebDAV endpoints for storage systems secured with POSIX permissions.

This module is a little-modified fork of the [`mpm-itk` module](http://mpm-itk.sesse.net/).
The minimal change is the stage at which HTTPD performs the user switching, so that authentication results from other modules (`mod_auth_basic`, `mod_oauth2`, ...) are available for actions.
Also, there are fixes for security issues under WebDAV and for running in Docker containers.

## Configuration

The module must be loaded with the usual `LoadModule` statement and no further global configuration is required:

```
LoadModule mpmitk_setuid_module modules/mod_mpmitk_setuid.so
```

To make use of the `setuid` functionality, `AssignUserID` or `AssignUserIDExpr` have to be specified.
Configuration directives:

 * `AssignUserID <user> <group>` (context: server config, virtual host, directory):
   Switch to the specified user and group.
   Either those are names which are looked up in the entity database or numeric IDs in the format `#<num>`.

 * `AssignUserIDExpr <expr>` (context: server config, virtual host, directory):
   Evaluate the [expression](https://httpd.apache.org/docs/2.4/expr.html) and switch to the user wit the resulting name.

   `AssignUserIDExpr` effectively overwrites `AssignUserID`.

 * `AssignGroupIDExpr <expr>` (context: server config, virtual host, directory):
   Evaluate the [expression](https://httpd.apache.org/docs/2.4/expr.html) and switch to the group with the resulting name.

   `AssignGroupIDExpr` effectively overwrites `AssignUserID`.

 * `LimitUIDRange <min_uid> <max_uid>` (context: server config):
   Restricts `setuid` calls to only accept UIDs within the specified range.

   The filter is applied early, so the user user specified in `User` must be covered, otherwise `mod_unixd` will not switch away from root (relevant with enabled capabilities).

 * `LimitGIDRange <min_gid> <max_gid>` (context: server config):
   Restricts `setgid` calls to only accept GIDs within the specified range.

 * `EnableCapabilities On|Off` (context: server config):
   If capability support is compiled in, this will enable/disable the use of capabilities for enhanced privilege management.
   See the [Capabilities](#capabilities) section.

 * `MaxClientsVHost <num>` (context: server config):
   Limit the number of active clients per virtual host.
   If another client attempts a request on the virtual host it will receive a 503 Service Unavailable response.

 * `NiceValue <num>` (context: server config, virtual host, directory):
   Change the nice process priority setting for request handling processes.

The module **must** be combined with the `prefork` MPM.
Directives of the prefork MPM control how many processes are forked and ready for request handling.
Also see `mpm_common`.

 * https://httpd.apache.org/docs/2.4/mod/prefork.html
 * https://httpd.apache.org/docs/2.4/mod/mpm_common.html

## Approach

The module only takes action if one of the `AssignUser*` directives has been specified.

Switching the request handling process' user and group takes place in the `ap_hook_type_checker` hook, which is the first core hook after authentication and authorization has completed.

Once the user and group has been determined from the `AssignUser*` directives, the module will perform syscalls to switch the user and groups which the process runs as.
These operations are possible because the request handling process retains elevated privileges, see [Capabilities](#capabilities) section.
A `setgid` call is performed to change the GID of the process.
An `initgroups` call is performed to load group membership of the user from the Linux Entity `group` Database and set these as supplementary groups.
A `setuid` call is performed to change the UID of the process.

Under the `prefork` MPM, a single process handles each connection.
Once the request handling process has switched to a specific user, there is no way to gain more privileges and no way to switch to another user.
Usually, that is fine because subsequent requests on the same connection will likely contain the same user credentials and the process can continue to serve these requests.
If the process receives a request which should run as another user (determined after authentication and authorization), the process will exit meaning the HTTP connection is closed.
According to the specification, clients should repeat the request in this situation, although implementations vary.

Multiple requests on the same connection are also the reason why early phases of request handling (including authentication and authorization) may be executed in processes running as various users.
The first request of a request handling process will run under the user specified by `User` (assuming enabled capabilities) and subsequent requests on the same connection will be started to be handled with the user determined for the first request.

## Capabilities

The module must be built with libcap available on the system to make use of the enhanced privilege mechanism based on capabilities.
In Docker, use the `libcap` tag of the module builder image.
RPMs are always built with `libcap` support.

The `EnableCapabilities` directive enables the capability-based mechanism (the default is enabled).

The following capabilities are used:

 * `CAP_SETPCAP` for amending secure bits of the process to keep capabilities across a setuid and then dropping capabilities.

 * `CAP_SETUID` for performing setuid.

 * `CAP_SETGID` for performing setgid.

 * `CAP_DAC_READ_SEARCH` (preferred) or `CAP_DAC_OVERRIDE` for bypassing file access controls during early request handling.

 * `CAP_SYS_NICE` (optional) for raising the process priority (decreasing niceness) of the request handling processes with the `NiceValue` directive.

   The capability is only required when the priority is increased above its initial value.

## Security Considerations

The HTTPD main process runs with full root privileges (as usual).
The `prefork` MPM forks processes for request handling from the main process.

 * Without enabled capabilities, the request handling processes continue to run with the root user until after authentication of the user.
   Then, they switch (`setuid`) to a user based on the `AssignUserId` and similar directives.

   `mod_unixd` is effectively disabled.

 * With enabled capabilities, `mod_unixd` immediately switches (`setuid`) to the user configured by the usual `User` and similar directives.
   However, elevated capabilities (described above) are kept in place to allow later switches of the user.

   Further processes launched (`execve`) do not receive capabilities.

   After authentication, the process switches to the user based on the `AssignUserId` and similar directives.
   In addition, the capabilities are dropped at this stage.

**Running with capabilities is absolutely recommended.**

### seccomp

The `LimitUIDRange` and `LimitGIDRange` instructions install a filter on the setuid syscall.
It only allows switching to a user/group where the numeric id falls within the configured range.

Future work: Use securebits and the bounding capability set to ensure that root capabilities cannot be regained.

## Releases, Packaging and Distribution

`mod_mpmitk_setuid` is released through tags in the Git repository.
The versioning scheme follows [Semantic Versioning 2.0.0](https://semver.org/).

The initial released version starts `v3.0.0`, to signal a departure from MPM ITK's use of the compatible Apache HTTPD version as the basis of its versions.
`mod_mpmitk_setuid` in version `v3.0.0` is compatible with Apache HTTPD 2.4.7 and newer.

RPMs are built in GitLab CI and made available as job artifacts.
These artifacts can be downloaded from the "Tags" page in GitLab by clicking the download icon next to a version tag and selecting the appropriate job, e.g. `package:rpm:centos7` for Enterprise Linux 7.
Versions of the RPMs follow the versions of the module with modifications for proper ordering.

A module builder Docker image is also built, but there is no tagging in place yet.
See the test configurations for how to use the image and see the HTTPD Docker repository for background.

### For Maintainers

Releasing a new version requires amending the version in several places:

 * `mod_mpmitk_setuid/configure.ac` in `AC_INIT`
 * `mod_mpmitk_setuid/mod_mpmitk_setuid.c` in `MPMITK_SETUID_VERSION`
 * `rpms/httpd-mod-mpmitk-setuid.spec` in `upstream_version`

`autoconf` has to be executed in `mod_mpmitk_setuid/` to generate a new `./configure` file.

These changes have to be committed and this commit tagged with `v${VERSION}`.

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the Apache License, Version 2.0 (see LICENSE file).
Detailed authorship information is part of the Git commit data or included in the files directly.

```
Copyright 2005-2023 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
